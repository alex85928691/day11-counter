import Counter from "./Counter";
import {useSelector} from "react-redux"


const CounterGroup = () => {

    const countList = useSelector(state => state.counter.countList)

    // const handleChange = (index, value) => {
    //     const updateConterList = [...counterList];
    //     updateConterList[index] = value;
    //     onChange(updateConterList);
    // }

    return (
        <div>
            {
                countList.map((value, index) =>
                <Counter key={index} index={index} value={value} ></Counter>)
            }

            </div>)

}

export default CounterGroup;