import { createSlice } from "@reduxjs/toolkit";

export const counterSlice = createSlice({
    name:'counter',
    initialState:{
        countList:[],
    },

    reducers:{
        increment: (state,action) =>{
            state.countList[action.payload] +=1
        },
        decrement: (state,action) =>{
            state.countList[action.payload] -=1
        },
        counterSizeChange: (state,action) =>{
            state.countList = Array(action.payload).fill(0)
        }

    }

})

export const {increment,decrement,counterSizeChange}= counterSlice.actions

export default counterSlice.reducer