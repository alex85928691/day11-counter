import {useSelector,useDispatch} from "react-redux"
import { counterSizeChange } from "./app/counterSlice"

const CounterSizeGenerator = () => {

    const countList = useSelector(state => state.counter.countList)
    const size = countList.length
    const dispatch = useDispatch()

    const handleChange = (event) => {
        dispatch(counterSizeChange(Number(event.target.value)))
        // onChange(Number(event.target.value))
    }

    return (
        <div className="counterSize">
            <label>Size: </label><input onChange={handleChange} type="number" value={size} min={0} ></input>
        </div>
    )
}


export default CounterSizeGenerator
