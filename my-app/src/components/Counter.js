import {useDispatch} from "react-redux"
import { increment,decrement } from "./app/counterSlice"

const Counter = ({index, value}) =>{
    const dispatch = useDispatch()

    const handleIncrease=()=>{
        dispatch(increment(index))
    }

    const handleDecrease=()=>{
        dispatch(decrement(index))
    }
    return(
        <div className="counter">
            <button onClick={handleIncrease}>+</button>
            <span>{value}</span>
            <button onClick={handleDecrease}>-</button>
        </div>
    )
}

export default Counter;