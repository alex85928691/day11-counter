import './App.css';
import MutipleCounter from './components/MutipleCounter';

function App() {
  return (
    <div className="App" >
      <MutipleCounter />
    </div>
  );
}

export default App;
